This diary file is written by Javier Segarra RA6078464 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.
16h Class: Second day of presentations and exam

#19th of June#

* I found the exam had some easy questions and others a bit more complex, where you needed to have more time. Not complicated to score a 60, but maybe more difficult to increase the results
* The second group made a quite interesting presentation
* After the presentations, I think my group would have more inspiration and might be able to improve a bit more the code we presented, as each one of us had dealt the problem from their own point of view
* Overall I have to say me and my group did a very good job, I am proud of.
* Again, just wanted to say a great course, eventhough it was too short, it has been enough to realise I want to keep on learning about it.