This diary file is written by Judy Chen C24051091 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2019-02-20 #

* The first lecture was great.
* I am looking foward to search for how AI can outperform human.
* I am still curious about how AI read lips.


# 2019-02-27 #

* I think that statistic is science. It applies to express the distribution of things in real world.
* I got the knowledge of the relationship of AI, ML, DL
* I still don't know if ML is not explicitly programmed, can we understand how it managed to learn?
* I am thinking why is that only Deeplearning has multiple layers. Don't all the AI neuro networks necessarily need multiple layer structure?


# 2019-03-06 #

* We discuss how to write our diary entry
* I get more clear about how markdown syntax works
* However, I still want to get more knowledge of the branches
* We get a concise python tutorial
* I took a programming course before, but first time trying python
* I think it will not be too hard. If I want to print something, the function is just print()
* I just need to get used to some different language usage
* This is the first time I learned that boolean and integer can add together
* I think I'll need some time to learn reading loops, because there is no big parenthesis
* I think triple quoted string is more convenient than putting a backslash before quotes in strings
* But how do we maje severl comment lines? Should we put '#' in front of every line?

# 2019-03-13 #

* In today's lecture we do practice of python code with Jupyter Notebook in a playground mode
* I think the playground mode is a great way to learn programming
* The professor made us practice in pairs, it's a best way to learn faster and to check if I know how to code and solve the given exercise
* I learned the loops basics before, so I jump foward to the class & inheritance part
* I haven't finished all the exercise during the class
* I'll figure out how to use "__init__" in defining functions after class
* I'll study it with the video tutorial link given by the professor, I think the videos are helpful
* I'll download and install AnacondaVScode after class
* It's interesting because I've used Visual Studio before but never known VScode
* And there's a Deeplearning tutorial in the AI_Public file, I'll try reading it this week

# 2019-03-27 #

* Machine learning can use feature of the samples to learn to approximate the best result
* such as it can use the grayscale value of each pixel to determine the image
* we learn the basics of capacity and hypothesis space
* there are strength and weakness of capacity
* the weakness is that it needs big enough data set to train the neural network
* but thanks to the bigdata we can do more work 
* about the hypothesis space: the more layer of the neural network means larger capacity 
* Today we have an interesting question in the lecture:why is every model wrong?
* In physics, the Heisenberg uncertainty principle states that we can't measure the true state of a system
* Therefore, we discuss that the aim of a model is not to represent the real system
* I think it's because the model is what we create to fit the real system when we don't know what it really looks like

# 2019-04-10 #

* I spent some effort to catch on the idea of CNN
* I get the idea of that deeplearnig learns by finding smaller error
* And I think that I still don't get why the filter scan in that way

# 2019-04-17 #

* In today's lecture we get an introduction to Tensorflow
* The professor told us that there's an alternative for Tensorflow, that is pyTorch
* However, if not for special research, Tensorflow is still the better one for beginers
* We discuss that the steps for building up a digit determining model is simple
* And we are asked if it can do other things than recognizing digits
* I simply get the answer 'no'
* Because I think the model is trained only for the 10 digits, which is defined in the second layer of the model
* I think it is good for others to bring up the idea of how we can make the model do other things
* Then I get new information of other learning and training methods also 

# 2019-04-24 #

* In today's lecture we try to run the Tensorflow code on the playground
* I finally realize it is really convenient to import class functions we need from Keras first
* Therefore our code will be more compact, becasuse we don't need to call it again and again
* In the exercise, I'm curious what is Relu activation function so I look up on the Internet
* Still not so sure what is the advantage of using it than the sigmoid function
* So I ask the professor after class
* I get a small conclusion:
* The Relu activation function do the work better than the sigmoid function
* Because the sigmoid function will be very sensitive for large input
* And that the Relu function don't need to worried about the scalling
* Also, using the Relu function will make the computation more easier
* So the Relu function can do the work faster, too

# 2019-05-01 #

* It is really good that we discuss the code of deeplearning model line by line in class
* I get more clear with how the model was built layer by layer
* But still have question about if we have two hidden layers with different numbers of nodes, it should work different with those of same sumbers of nodes
* I'll try out whether if it will improve accuracy or scanning rate, or just make the model's condition worse
* When discussing about which activation function, which optimizer to use, professor told us it is the art to build the model not the science
* I suppose that's because deeplearning is still a black box to us today
* I think there are so much calculation behind, it should have some reason why this method is better for the specific result we want to get
* And for every method there are pros and cons, even nothing is the best we still need to minimize the total loss 
* I want to learn to loading different image to train the model

# 2019-05-08 #

* In today's lecture we reviewed the course objectives and checked if we'd reached them
* I found out I just get to know the ideas but not clearly understand them, I should go back and review the videos
* I think it is really good for this course to give us a big picture of Deeplearning
* Because I'm not going to be an enginner but it may be a helpful method with my work 
* I think there are two reasons why I still consider Deeplearning difficult
* First, I should review the basics ideas as I mentioned
* Second, we are given a simple deeplearning model code and also tried some changes
* but if I want to make an adaption, I don't know what are the available functions or modules to import when using tensorflow

# 2019-05-15 #

<<<<<<< HEAD
* I think automation can really solve the rich and poor gap by increasing wage per person
* With automation, people don't need to do hard and trivial work. 
* We will have more brains thinking ways to improve our society or solving environmental problems 

# 2019-05-22 #

<<<<<<< HEAD
* Today we watch tedtalks on interesting AI application topics and what should be improve
* I'm surprised by that the AI face recognition software cannot recognize the face of dark skin people, that's so unfair
* I think the prediction mistake is more acceptable if the task doesn't involved with human feeling or self-esteem

# 2019-05-29 #

* I'm feeling great with my group member 
* We're both not so good at coding AI but willingly to give our best try
* We discussed we can try out the multi-column DNN method 
* And it's good we discussed with another classmate who hasn't found his group members
=======
* In today's lecture we watch some tedtalks on interesting application topics of AI prediction
* AI's prediction is no doubt helpful in many ways, but there are still many improvement should be made
* Such as Image recognition, AI may sometimes make a totaly mistake if the image is not an object but a snapshot of an event
* I think the probability of AI's prediction mistake is more acceptable if only the task is not involved with human feeling
* I'm very surprised by that the AI face recognitionn software cannot recognize the face of dark skin color people in the begining, that's so unfair
* I do agree with the first talker that if the engineers improve the coding of AI, people may live a life more equally with the technology's help
>>>>>>> origin/Judy-Chen/judy_chenmd-edited-online-with-bitbucket-1558518625768
=======
* I've never heard about 4th industrial revolution  
* I think that automation in some boundaries do have advantages, so we don't need to worry about we don't have enough job for everyone
* I really agreed with that automation would increase wage is really helpful to decrease the gap between the poor and the rich
* Imagine with automation to do hard works and trivial works, we will have more brains thinking ways to imporve our society or solving environment problems

>>>>>>> origin/judy453378/judy_chenmd-edited-online-with-bitbucket-1557935761353


