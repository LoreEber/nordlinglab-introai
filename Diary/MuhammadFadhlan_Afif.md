This diary file is written by Muhammad Fadhlan Afif N16097017 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2019-03-19 #

* There are video links in explanation of Lecture material and Assignments section that leads to the same video (I believe it supposed to lead to different video).
* I believe that news, information from medias, movies and science-finction maybe lead me to wrong understanding about AI.
* I think the annalogy in between 'AI' and 'Biology' is great. AI is dicipline or collection of concepts, problems, and methods for solving certain task.
* I think that the task, which is more need intuitive aspects, will be harder to be solved or done by AI.
* On the other hand, tasks, which required logics or probabilites or calculations, is more easy to be solved by AI.
* I also learnt that we cannot compare AI systems with different task.
* I hope that learning further about AI may develop my ability to cooperate with people who really focus on it.
* I read the article "Design and Implementation of Heuristic Intelligent Attack Algorithm in Robot Soccer System Small Size League"

# Article summary #
* Various collaborative attacker strategy successfully implemented
* Various collaborative attacker strategy were evaluated by counting the successful trials in five trials experiment each.
* These collaborative attacker strategy were recommended to be used in certain game situation or scenario
* Optimizing the performance of AI sensing accuracy by vision system towards a better and more precise
* Combining this collaboration strategy with defense collaboration strategy with a combination of a lot more various dynamic games