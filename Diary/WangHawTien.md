* Haw-Tien Wang , student number F44061018, dimitriwang97@gmail.com, Introduction to AI 3/6 Diary

*Today we learned a new programming language--Phyton.
*We got basic knowledge about loop statement, which is very important.
*Now, we are more familiar with the Bitbucket. 
*We are noted that we have to use Python version 3.

For my part, the most important thing in today's class was that, I was tought to define something on my own.
As shown in today's powerpoint, many things , inclusive of AI, have more than one definition. Let's take statistics
for example, it has a great amont of different definitions from different books, by different authors , in different
prospectives. Even some of them hold contrast view toward the same thing. Thorny it may make us feel, however, we can 
highlight some key words or key concepts which are mutually mentioned among them. By doing so, we can grasp all of 
the main properties featuring the thing we want to define. Eventually, we can derive our own definition.
This, was the very first time I was required to give my own definition on something. Thanks my teacher a lot. That was
provoking. Back in the 7 years, or even 13 years(counting my elementary school era) of my studying, there was not a
single teacher to ask me about my own definition on something. In conventional Asian education , we are asked to obey
the definition given, and based on it , to expand it, to apply it. Consequently , we never 'think', we never 
reasoning on what we are using. Let alone coming up with our on view on something or creating something not just
brand new but revolutionary new.