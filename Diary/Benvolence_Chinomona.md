This diary file is written by Benvolence Chinomona N18077021 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2020-03-12 #

* Read the success stories of AI in breast cancer screening. 
* Article: International evaluation of an AI system for breast cancer screening
# Article summary #
* Screening mammography can identify breast cancer at earlier stages when treatment can be more successful1
* AI(biopsy) was developed using UK data and performance of the AI system was compared to that of six independent radiologists using a subset of the US test set
* The AI system exceeded the average performance of radiologists by a significant margin (change in area under curve (ΔAUC) = +0.115, 95% CI 0.055, 0.175; P = 0.0002)
* Results shows that the sensitivity advantage of the AI system is concentrated on the identification of invasive cancers (e.g.invasive lobular or ductal carcinoma) rather than in situ cancer (e.g. ductal carcinoma in situ)
