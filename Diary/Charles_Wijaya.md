This diary file is written by Charles Wijaya E34085303 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2020-03-18 #

* Watch the welcome video.
* Read success stories of AI.
* Find out that AI can be applied in almost all subject.
* AI can even interpret human emotion.
* Emotion interpretation is done by monitoring human facial expressions and body temperature.
* AI also gives solution to the farmer in the agriculture field.
