__This diary is written by Yeoh Chuin Shung (Mario) of student ID C64075302.__    

__2019-2-20__    
    *    I think the first lesson was great as I can get an overview of the course for the next 17 weeks.    
    *    I was really glad that this course uses English as the main language.
    *    I really wanted to learn more about AI, especially building one of my own, which really excites me.    
    *    It was really mind-blowing when I got to know the professor has research on bioinformatics.    
    *    It was also jaw-dropping that the professor knows quite a lot in biology even though he comes from a total Physics / Maths background.    
    *    I really considered about visiting the professor's lab as I am from the Biotech department and Bioinformatics is part of the course as well.


__2019-2-27__    
    *    I never knew the field of artificial intelligence has many incorporated parts of other fields.
    *    It was interesting to finally know the differences between machine learning and artificial intelligence.  
    *    I would rather classify statistics as a subset of math than science.  
    *    My question of the day, since AI technology is mainly based on statistic and the processing of existing data, can creativity and innovation be incorporated? 
    *    Since AI is meant to help us on certain task, can it be used to clean up data mined instead of us?  
    *    I think everyone should be aware of the development and growth of AI technology.   

__2019-3-6__    
    *    I think today's lesson on Python language writing is a little too fast.    
    *    I had personally learnt Python before, thus I think the syllybus for this lesson is acceptable and understandable.    
    *    For me, I would rather go with identation than brackets (seen in Java) as brackets make the whole code looks longer and messier.    
    *    It was great for the professor to tackle the problem most of us faced while entering our diary last Wednesday.    
    *    I really think that bitbucket has to have an easy guide or walkthrough provided to new users of their services as the UI is hard to understand or operate.    
    *    The lesson today focused more on solving the problem most of us faced last week, so we didn't really had enough time to finish the core lesson, which is a pity.     
    
__2019-3-13__    
    *    I think it is the right way for us to learn python by trial and error.    
    *    I think the bad part about this lesson though is the Wifi connectivity.    
    *    I think I will stick to Jupyter Notebook as my primary code editor, as I have a lot of experience with it.    
    *    It would be great of the professor would teach us how to navigate the UI of VScode as VScode has a few more pros than Jupyter Notebook.    
    *    It was also facinating to see Google Drive can support a .ipynb file for learning python on a python notebook.    
    *    The lesson was overall nice, as the assistants are helpful and we get to play around with the code.    
    
__2019-3-20__    
    *    I think neural network is an artificial thinking pathway, based on the neural network of human brains.    
    *    Simply speaking, neural network consists of mutliple layers, each of which determines wether the inputs given matches the required criteria in numbers 0-1, whereby 0 means totally no, 1 means totally yes.    
    *    After watching the video played in class, I have a gain a rather thorough insight on what neural pathway is and how it is being developed, using math algorithms.    
    *    It is also amazing to know how much work that is required to programme a workable and simple neural network.    
    *    It is quite complex to train a neural network as there are many ways to test the system.    
    *    I think I need to read through the slides more carefully again to take everything in that has been taught by the professor.    

__2019-3-27__    
    *    I never knew machine learning requires a balance between the chosen parameters and the filtering of it.    
    *    From this lesson, I learned that the filtering of data sets require a balance between the bias and the variance.    
    *    I have also learned that we can use under- or overfitting to filter out preferred output.    
    *    The aim of creating a model is not to be the truth, but to be a good approximation to the truth, hence models are always wrong.    
    *    Algorithms are meant to be useful for the users by fitting the the particular situation as accurate as possible, not to be reality itself.    
    *    A neural network is based on 3 points: weigh, sum up and output (activation functions).    
    
__2019-4-10__    
    *    I learned that the stochastic gradient descent is a core algorithm in most deep learning, which requires the serch for best optimal pathway to take while navigating the landscape.    
    *    This landscape is somesort of a 3D map where a global minimum exists, though it is not preferable to find the global minimum as all the noise are being included, hence making our model being overfitted.    
    *    The learning rate can affect our chances to find to global minimum, being too large, we may miss it, being too low might get us stuck in the local minima. There also includes the problem with the time taken.    
    *    I wonder if it is necessary to use both methods to filter thorugh the data set while training our AI, since it requires lots of times and the final results may be intimidatingly complex.    
    *    I think convulational method for image differentiation is quite accurate, provided if the weight and bias selection is closer to the optimum value, as well as the size of our datasets.    
    *    I think pooling layers is like the targeting tool for AI to detect specific thing that we want, in the form of high value pixel counts in the final output, as of which has to be compared to our huge dataset for confirmation of target.    
    *    I think the selection of the tools and methods are important for the optimization and generalization of our AI model, which in itself requires a huge amount of dataset and many trial and errors.    
    
__2019-4-17__    
    *    I am excited that we can start using TensorFlow in our python program to make a workable model.    
    *    I hope that the installing process for TensorFlow and mnist won't be a pain.    
    *    I think the syntax for using TensorFlow and MNIST is not as complex as I had previously thought so.    
    *    In my opinion, the steps required to make a workable model is rather easy, but it would still take time to train the model to make it accurate.    
    *    It is good to know that there are other alternatives than TensorFlow 2.0, which have their own strengths, like PyTorch ( better for research use ).    
    *    Since TensorFlow is backed by Google, no wonder it is one of the best machine learning module in python.    
    *    I was shocked that our model we are going to make is considered as an artificial narrow intelligence, rather than an artificial general intelligence.    
    *    With that said, our model is only good in what it is trained to do, differentiate numbers.    
    *    If we want to make our model work on something else, we can use data transfer and retrain the model with a whole new dataset.    
    
__2019-4-24__    
    *    I think it is awesome for us to be able to try writing our first model using python and Tensorflow.    
    *    The professor and his students were very helpful in helping us through our code writing session, which I am very grateful for.    
    *    I managed to solve the numpy issue on my laptop, with the help of the internet and updating the module to a new one.    
    *    Even though what I thought was the only problem was solved, another one popped out after I finished running the programme.    
    *    It really puzzled me about why I can't save my model in (.h5 file), as an error of "non-UTF-8 coding" keeps popping out.    
    *    I was grateful for the master student to help me solve my problem, though it wasn't totally solved, but at least it gave me some direction to work it out.    
    *    I am very excited that we can put this model into test and eventually work on a locally-hosted website.    
    
__2019-5-1__    
    *    I think writing the programme is not hard, as there are many resources online on how to use Tensorflow.    
    *    I wonder if I can write a code to allow the GPU to share the load of creating the model which is essential for our exercise.    
    *    I totally agree that we need to get our hands "dirty" to effectively learn programming.    
    *    I am grateful to the teacher for going through the whole programming part one by one as it is important and helps clear up some of my questions.    
    *    I actually took me a long time to complete the whole exercise, including the Flask part, as there are many parts needed to be troubleshooted.     
    
__2019-5-8__    
    *    I think that it is great for the professor to give us a quick revision of what we have learned in this course up till now.    
    *    Artificial neural network is a model based closely on the human neural network, which consists of many filters which allows inputs to be filtered to give the required output.    
    *    Convolution can be defined as something which is complex, which is true for convolutional neural network, as it is a class of deep neural networks that is commonly used for  analyzing visual imagery.    
    *    CNNs are regularized versions of multilayer perceptrons, whereby multilayer perceptrons can be defined as fully connected networks.    
    *    Backpropogation algorithms are a family of methods used to efficiently train artificial neural networks following a gradient descent approach that exploits the chain rule.    
    *    I think it is true that deep learning is hard, even though there are many tutorials that teaches us how to use it, but we may still not be able to understand it fully, i.e. how they work.     
    *    I think that trial and error is very important to find the suitable architecture of the neural network, as the design of a neural network architecture is still and may always be an art.    
    *    I totally agree that by mastering programming, we can then let our imagination run wild to make the model that we want, as there is no general formula to make a good neural network, only time and countless of tries will make a better neural network.     
    
__2019-5-15__    
    *    I was never aware of the fact that we have 3 industrial revolution, needless to say a fourth one.    
    *    I think that the use of renewable energy actutally depends on many factors, like the natural geography of the particular country and of course the wealth of the country.    
    *    I think that AI can be used for many things, as its prediction capabilities is getting better each day, but then it is also wise to respect the power of the internet and the privacy of its users.    
    *    There are many complications that come with the development of AI, and I certainly believe that we need to have certain regulations in place to uphold the basic rights of humans.    
    *    With the exponential growth of AI deveplopment, I think that no jobs will be safe, but maybe jobs that require creativity and out-of-the-box thinking could still be safe.    
    *    As far as I am concerned, I believe that human-created algorithms can only be as good as the human itself.    
    *    I wonder, if it is possible to design an AI that could predict the stock market.    
    
__2019-5-22__    
    *    I believe that ethics in the field of artificial intelligence has to be well established by a selected global committee, just like those in the field of medical science and biotechnology.    
    *    I think that algorithmic bias is an extension of human bias into computers.    
    *    Algoritmic bias can be solved by using proper datasets and proper coding, but there is always the problem of the willingness of programmers and developers to make the change.    
    *    The consequences of algorithm bias can vary, but I believe that for the current state of development in the technology, the outputs or predictions from these machine learning softwares should only be used as  purely a referrence.
    *    The use of such softwares in the judistical department and law enforcement department should be heavily regulated, with an independent global organization regulating the development and training the model, to provide a much accurate and relatively "unbias" output.    
    
__2019-5-29__    
    *    I was a little shocked when I came in because I forgotten about the random group assignment related to the group project.    
    *    I think it will be fun for us since we can play with the code and create our own more accurate neural network.    
    *    Typically, I think that by adding more layers, we can increase the accuracy of our neural network.    
    *    Well, certainly, when there is a group project, we need to spend time for multiple meetings, which is going to be tiring.    
    *    I do hope that we can at least meet all the benchmark requirements for this project, and have some fun learning along the way.    
    *    I think our team is up for the job and I do believe we can collaborate with each other easily and efficiently.       
    
__2019-6-5__    
    *    I think that the benchmark accuracy score that the professor gave is a little hard to beat, but it is not impossible.    
    *    I think it would take some serious training and multiple trials and errors to make our model much better.    
    *    I do reckon that this work would be straining my laptop.    
    *    I do hope that we can get a better model soon, or else we might fail this.    
    *    Well, I do believe that I have hit the jackpot by tumbling upon some blog post on how to improve a model, even though it is not that comprehensive.    
    *    In the near future, I think I would be utilizing my laptop's processor well, but I might borrow some friend's better laptop for better speed.    
    
__2019-6-12__
    *    I think today's presentation is very informative, especially about the artificial intelligence benchmark.    
    *    I believe that everyone did a great job in their presentation, though there are those that stood out, in terms of their model accuracy.    
    *    I think that the results we got on our model in particular is rather peculiar, even though it beats everyone else's score.    
    *    The presentation duration is rather short in my opinion, but it is still acceptable, since it has forced everybody to get to the point of their presentation.    
    *    This course is one of the most enjoyable ones I have enrolled in so far to date, mainly because it speaks two of my beloved languages, English and Python.    
    *    I am looking forward for more or some similar course, maybe with a little more difficulty, but certainly has to be in English.    
    
__2019-6-19__    
    *    It is cool to see a group to use another type of neural network architecture ( AlexNet ), and the result they got is quite high.    
    *    The second team that presented used "dropconnect" instead of "dropout" to reduce the chance of overfitting, hence improving accuracy, which is quite cool.    
    *    For the third team that presented, I think they had spent quite some time to make their PPT a lot better than most of us, which is nice.    
    *    They managed to get a rather high accuracy for their results as well.    
    *    As the final group to present, I think we did well.    
    *    For us, we have to troubleshoot our code as we had got a bug in our final results, but eventually we solved it and our result is quite the norm, of course improved from the benchmark.    
    *    I truly think that I might be swayed to attend a similar course again, with more difficulty and more credits, and the teaching medium is quite important too.    