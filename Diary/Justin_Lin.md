---

_This diary file is written by **Justin Lin E24086129** in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists._

---

# 2020-03-17 #

* Looking forward to learn AI and DL.
* I have learnt Python a little in this winter, that I wish to utilized it to develop artificial intelligence.
* I wish to know more about Python.
* I am trying to be familiar with [markdown syntax](https://www.markdownguide.org/basic-syntax/).

---

# 2020-03-18 #
* I've watched the welcome video.
* I've uploaded the success story of AI  
  * I wrote something about how AI combines IoT.
  * I also saw other examples on cloud which impressed me that AI can have so many implementations.

---




