This diary file is written by Martin Hodek P06088415 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2020-03-19 #

* Course had started, it is bit rough to get to know all the online stuff. I have never worked with bitbucked and moodle partly in Chinese is also quite puzzling.
* I have watched the intorduction video, read through Nordling Lab web and searched Bitbucket. Slightly overwhelmed. Homework comes next.
* While reading (and creating) AI sucessful stories (first homework), I have come to the conclusion, that if one can provide appropriate data set, on which AI can learn, it can then "quicky" perform various tasks over huge quantities of data, mainly conected to the data recognition. 
* AI can outperform humans in many fields, often due to higher computing capacity and sometimes due to greater "base knowledge", yet this should not be feared, but utilized.
* Still wondering, can AI get out of control, as it is depicted in Sci-fi? In my opinion no.