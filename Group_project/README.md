Your [Jupyter notebook](https://jupyter.org/) with both the training and prediction code, as well as the model specification and final model (saved in HDF5 format), need to be commited to this folder before the last lecture. Please name your files MNIST_Error%_FirstName_FamilyName after the group leader.

How to download your trained model:
![](https://i.imgur.com/HqtPQA0.png)  

How to upload the files to the group project folder:  
Choices:  
1. contact (me) Abraham Tan (spongepan2@gmail.com) and I will help you upload them. Please send them in a .zip file with the correct naming  
2. Learn how to use the git bash commands:  
i. Install the [git bash](http://opensourcerer.diy.org/challenge/3) and open it (as told in the website)  
((If you want to, you can continue learning from [this tutorial](https://www.atlassian.com/git/tutorials/learn-git-with-bitbucket-cloud) yourself. I will only teach the minimum to upload the files))  
ii. You should see something like this (with less stuff in it than mine)  
![](https://i.imgur.com/wlK455f.png)  
iii. Type (or copy and paste, you need to right click to do so):  
cd ~  
mkdir filename  
cd ~/filename/  
git config --global user.email "you@example.com"  

(I used 'at' as the filename and spongepan2@gmail.com for you@example.com  
So I typed:  
cd ~  
mkdir at  
cd ~/at/  
git config --global user.email "spongepan2@gmail.com"  

Note, the filename doesn't matter much)  


iv. Get the link for cloning the repository as below, paste it in the git bash and press return (enter)  
![](https://i.imgur.com/k2txENj.png)  

v. Now look for the folder with the filename you chose earlier (mine is named 'at') in your computer. It should be in your user folder.   
vi. In that folder there should be nordlinglab-introai folder and in it there is the Group_project folder.  
Copy and paste the files you want to upload into the Group_project folder.  

vii. Type and return 'cd ~/filename/nordlinglab-introai/Group_project/' in the git bash to tell it to go to that folder.  
((Note, 'filename' should be whatever you chose before))  
viii. Type and return 'git status' and you should see a list of files you want to upload. Copy and paste the texts into a notepad, it will help a lot.  
For example:  
MNIST_0.45%_Abraham_TAN_Group_6.ipynb  
MNIST_0.45%_Abraham_TAN_Group_6_MNIST_model.h5  
ix. add the text 'git add ' before each line of the text you copied earlier, then copy these and paste it into the git bash. You might need to press return for it to run.  
(You can paste and run multiple lines at once)  
For example:  
git add MNIST_0.45%_Abraham_TAN_Group_6.ipynb  
git add MNIST_0.45%_Abraham_TAN_Group_6_MNIST_model.h5  

x. Type:  
git commit -m 'Your commit message here'  
and return, this is to commit with the commit message 'Your commit message here' <-- change this of course  

xi. Lastly, type and return 'git push origin master' and it should start uploading. After it is done, you might need to wait a few moment before the one in the website updates.  

If you met any errors, try googling it or just try experimenting with it. Good luck.