
# Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists - 工程科學之人工智慧與深度學習導論 #


## Course description ##

Artificial Intelligence (AI) already impacts the life of essentially all internet users, but at the same time, it is Science Fiction. Deep Learning (DL) has since the ImageNet LSVRC-2012 contest established itself as one of the core technologies driving the third industrial revolution with many commercial applications. The success of the current wave of Artificial Narrow Intelligence (ANI) is due to:

* big labelled data,
* GPU accelerated distributed computing,
* open source software, and
* algorithms.

In this course, we will discuss what AI and DL is, their applications and implications from an automation perspective, the importance of ethics and danger of algorithm bias, and the methods that make AI possible, with a focus on Deep Neural Networks. You will also get to apply a Neural Network to identify numbers in images, using Python and TensorFlow. No previous knowledge of AI, Python, or TensorFlow is needed.

人工智慧(AI)已影響基本上所有的網路使用者，然而同時它也還是科幻小說的範疇。自從ImageNet LSVRC-2012競賽以來，深度學習(DL)已然成為一驅使第三次工業革命與其不可勝數的商業應用的核心科技，能有現在人工智慧(ANI)的成功，主要歸功於：

* 大標記數據
* GPU加速之分散式運算
* 開源軟體
* 演算法

在本課程中，我們將會討論人工智慧、深度學習、應用、自動化之觀點、倫理道德、演算法偏差以及實現人工智慧的方法。此外，本課程會有利用Python與Tensorflow神經網路判讀數字圖片的練習，課程前並不需要Python與Tensorflow的先備知識。

## Course objectives ##

1. Define Artificial Intelligence
2. Know some applications where AIs outperform humans
3. Understand some major implications of AI
4. Ability to critically examine AI news and claims
5. Understand the importance of ethics and algorithm bias
6. Explain some methods that make AI possible
7. Explain what an Artificial Neural Network is
8. Explain how Convolution works
9. Explain how Backpropagation works
10. Apply a Neural Network to recognise numbers

## Course outline (2020) ##

1.	Success stories of Artificial Intelligence. Introduction of the digit recognition case study.
2.	What is AI, Deep Learning, and Machine Learning?
3.	Introduction to Python (Getting help and comments, Printing, Conditional statements and loops)
4.	Introduction to Python (Variables and functions, Reading and writing files, Error handling)
5.	Introduction to Neural Networks and Modelling
6.	Introduction to TensorFlow
7.	Introduction to data preprocessing, model selection, regularization, activation functions, and neural network types, including convolution.
8.	Gradient descent, Backpropagation, and loss functions
9.	Introduction to Model validation (test/training split, cross-validation, under-/overfitting, bias variance tradeoff)
10.	Application of Deep Learning (group project)
11.	Ethics and the danger of algorithm bias
12.	Application of Deep Learning (group project)
13.	Automation and the future of work
14.	Application of Deep Learning (group project)
15.	Current AI research
16.	Final exam and group presentations

## Course outline (2019) ##

1.    Success stories of Artificial Intelligence
2.    What is AI, Deep Learning, and Machine Learning?
3.    Introduction to Python
4.    Python practice
5.    Introduction to Neural Networks
6.    Gradient descent and Backpropagation
7.    Convolutional Neural Networks
8.    Introduction to TensorFlow
9.    TensorFlow practice
10.    Ethics and the danger of algorithm bias
11.    Automation and the future of work
12.    Application of Deep Learning (group project)
13.    Application of Deep Learning (group project)
14.    Current AI research
15.    Application of Deep Learning (group project)
16.    Final exam and group presentations

## Teaching Strategies (2020) ##

* Using online teaching 60%
* Case study 20%
* Presentation 10%
* Video/music appreciation 10%

## Teaching Strategies (2019) ##

* Lecture 40%
* Discussion 10%
* Group project 40%
* Video/music appreciation 30%

## Course material ##

Short videos explaining a topic with companion lecture notes, suggested reading, exercises, and/or example code. All material is or will be freely available online. All material produced specifically for this course will be under the Creative Commons Attribution 4.0 International (CC BY 4.0) license with attribution to Prof. Nordling, Nordling Lab ([http://nordlinglab.org](http://nordlinglab.org)).

## References ##

1. Goodfellow, I., Bengio, Y. & Courville, A., 2016. Deep learning, Cambridge, MA, U.S.A.: MIT Press. Available at: [http://www.deeplearningbook.org](http://www.deeplearningbook.org).
2. O’Neil, C., 2016. Weapons of Math Destruction: How Big Data Increases Inequality and Threatens Democracy, Crown Random House. Available at: [https://weaponsofmathdestructionbook.com/](https://weaponsofmathdestructionbook.com/).
3. Jordan, M.I., 2018. Artificial Intelligence — The Revolution Hasn’t Happened Yet. Medium. Available [here](https://medium.com/@mijordan3/artificial-intelligence-the-revolution-hasnt-happened-yet-5e1d5812e1e7).
4. Purdy, M. & Daugherty, P., 2016. Why artificial intelligence is the future of growth, Available [here]( https://www.accenture.com/us-en/insight-artificial-intelligence-future-growth).
5. “Automate the Boring Stuff with Python.” [Online]. Available: http://automatetheboringstuff.com/. [Accessed: 06-Mar-2019].
6. “Learn Python the Hard Way.” [Online]. Available: https://learnpythonthehardway.org/python3/. [Accessed: 06-Mar-2019].


See [NordlingLab-AI-Public](https://bitbucket.org/temn/nordlinglab-ai-public/src/master/README.md) for a collection of links to useful courses, blogs, software, etc.  

## Course policy ##

1. Participation 30%
    Counted based on a weekly diary entry committed to the GIT repository before 24:00 every Sunday following [these instructions](https://bitbucket.org/temn/nordlinglab-introai/src/master/README_DIARY.md). Each student need to write down what he/she has learnt during the week. This is counted from the third week onwards.
    In the master and doctoral version of this course (N182200 / ME7122), each student need to read and summaries (in 3-5 bullet points) one recent open access scientific article on training of Deep Neural Networks using supervised learning. The summary must be included as the last entry in the diary file with header # Article summary #.
2. Group project 40%
    Graded based on final presentation during a synchronized (or physical) lecture, which should contain a demonstration of how well the trained Deep Neural Network performs on an independent test data set. A Jupyter notebook with both the training and prediction code, as well as the model specification and final model (saved in HDF5 format), need to be committed to the GIT repository in accordance with [these instructions](https://bitbucket.org/temn/nordlinglab-introai/src/master/Group_project/README.md).
3. Final exam 30%
    The exam will be conducted online using Google forms. The final exam will consist of questions with multiple choices.

## Course Website ##

* [GIT repository for sharing text files and code](https://bitbucket.org/temn/nordlinglab-introai/)
* [Google drive for sharing presentations and other material](https://drive.google.com/drive/folders/1HBzlkQm8hiOoJ_cFX9bFXbISjMYK-RR4?usp=sharing)
* [Nordling Lab - IntroAI web page](http://nordlinglab.org/introai/)
* [NCKU curriculum catalog](http://class-qry.acad.ncku.edu.tw/syllabus/online_display.php?syear=0108&sem=2&co_no=A93A600&class_code=)
* [Google sheet for signing up and reviewing scores](https://docs.google.com/spreadsheets/d/1hlizmVXF8F3ppyZS7bEl_VTyuMQqR2ug6-24uDl77y8/edit?usp=sharing)
* [Statistics of GIT commits, i.e. weekly diary score](http://statistics.nordlinglab.org/introai/)
* [YouTube playlist with course videos](https://www.youtube.com/playlist?list=PLrv1bMXfNtLYWUkZAkt5Mv4B88t7KC1dY) 
* [Nordling Lab - Python and Deep learning tutorial using TensorFlow 2.0 in Colaboratory](https://colab.research.google.com/drive/1uj3GEmGEJbJeaZEnJiUW0aTjtddkSK9o)
* [Nordling Lab - Python and Deep learning tutorial using TensorFlow 1.13 in Colaboratory](https://colab.research.google.com/drive/1dwMpKHgPU3GG4g_1XPIA8SF6QnglUwcl)
* [NordlingLab-AI-Public - Software installation for Deep learning tutorial](https://bitbucket.org/temn/nordlinglab-ai-public/src/master/deep_learning_tutorial.md)

# Contribution guidelines #

By adding material to this repository you certify that you have the right to add the material and that you want to make it publicly available under the [Creative Commons Attribution 4.0 International (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/) license with attribution to Prof. Nordling, Nordling Lab ([nordlinglab.org](https://nordlinglab.org/)).

## Who do I talk to? ##

If you have any questions start a discussion in Moodle. 
If it is course administration related email the Professor [professor@nordlinglab.org](mailto:professor@nordlinglab.org)

# Lecture material and assignments (2020) #

1.    Success stories of Artificial Intelligence. Introduction of the digit recognition case study.
    1. Watch
        * [Welcome video (YouTube)](https://youtu.be/-XyF-sJYiDQ)
        * [Success stories where Deep Neural Networks outperform Humans presentation video (YouTube)](https://youtu.be/-XyF-sJYiDQ)
            Abstract: Artificial intelligence (AI), in particular Deep learning (DL), has since the ImageNet LSVRC-2012 contest established itself as the leading Machine Learning technique with many commercial applications. This rapid success of Artificial narrow intelligence (ANI) is due to four factors: big labelled data, GPU accelerated distributed computing, open source software, and algorithms. Deep learning has within the last five years enabled computers to go from worthless to superhuman solution of many problems, such as skin cancer diagnosis, lip reading, image recognition, image description, and Go. However, computers are less power and data efficient. Training of artificial neural networks (ANNs) in general require thousands, if not millions, of examples, while humans can learn from a single example. In my lab, we are building smart toys to collect longitudinal data on how children learn with the aim of creating more data efficient training methods.
        * [Introduction of the digit recognition case study (YouTube)](https://youtu.be/-XyF-sJYiDQ)
    1. Sign up to participate in the course and get access to the material by registering in [this Google Sheet](https://docs.google.com/spreadsheets/d/1hlizmVXF8F3ppyZS7bEl_VTyuMQqR2ug6-24uDl77y8/). The purpose of this list is to enable you to use your existing account(s) and to see who is actually following the course.
    2. Fill in the survey about your background and expectations in [this Google sheet](https://docs.google.com/forms/d/e/1FAIpQLScSm1O3wGEpbh8TetnrtwwlENhGZPZ6N23GZRr1JqnvBEwSzw/viewform?usp=sf_link).
    3. Find a success story where an AI outperform humans
        1. Read the success stories added in the folder SuccessStories in the [course Google Drive](https://drive.google.com/drive/folders/1HBzlkQm8hiOoJ_cFX9bFXbISjMYK-RR4).
        2. Find one additional unique success story where an AI outperform humans or at least is useful for performing a specific task. Use [Google](https://www.google.com/), [Google Scholar](https://scholar.google.com/), [Scopus](https://www.scopus.com/search/form.uri?display=basic), [Arxiv](https://arxiv.org/search/) to find information about the AI.
        3. Prepare 1-3 PPT/KEY slides about the AI using the Nordling Lab template available [here](https://drive.google.com/open?id=1svIqjZkx0IaJ7hZUGhZhs4eGmd2qZGh7). Focus on showing pictures of which problem the AI solves and how the performance of the AI compare to humans.
        4. Upload the slides to the folder SuccessStories in the [course Google Drive](https://drive.google.com/drive/folders/1HBzlkQm8hiOoJ_cFX9bFXbISjMYK-RR4). (Everyone that has filled in the [Google sheet for signing up and reviewing scores](https://docs.google.com/spreadsheets/d/1hlizmVXF8F3ppyZS7bEl_VTyuMQqR2ug6-24uDl77y8/edit?usp=sharing) will get write access to the [course Google Drive](https://drive.google.com/drive/folders/1HBzlkQm8hiOoJ_cFX9bFXbISjMYK-RR4) on Tuesday afternoon (March 17th). Do not email any files to the teacher or TA.)
        5. Use a filename on the format YourFamilyName_YourStudentNumber_Short_description_of_success_story, e.g. Wu_E1000000_Image_captions.pptx.
        6. Ensure that you picked a different success story than all the previously added ones in the folder. A list of success story topics used is found [here](./SuccessStories.md). Please add your topic and correct any errors.
2.	What is AI, Deep Learning, and Machine Learning?
    1. Read [Elements of AI - Introduction to AI - Chapter 1: What is AI?](https://course.elementsofai.com/1) (You don't need to do their exercise, because it requires registration.)
3.	Introduction to Python (Getting help and comments, Printing, Conditional statements and loops)
4.	Introduction to Python (Variables and functions, Reading and writing files, Error handling)
5.	Introduction to Neural Networks and Modelling
6.	Introduction to TensorFlow
7.	Introduction to data preprocessing, model selection, regularization, activation functions, and neural network types, including convolution.
8.	Gradient descent, Backpropagation, and loss functions
9.	Introduction to Model validation (test/training split, cross-validation, under-/overfitting, bias variance tradeoff)
10.	Application of Deep Learning (group project)
11.	Ethics and the danger of algorithm bias
12.	Application of Deep Learning (group project)
13.	Automation and the future of work
14.	Application of Deep Learning (group project)
15.	Current AI research
16.	Final exam and group presentations


# Lecture material (2019) #

1.    Success stories of Artificial Intelligence
    * Lecture notes
2.    What is AI, Deep Learning, and Machine Learning?
    * Lecture notes
3.    Introduction to Python
    * Lecture notes
4.    Python practice
    * [Nordling Lab - Python and Deep learning tutorial in Colaboratory](https://colab.research.google.com/drive/1uj3GEmGEJbJeaZEnJiUW0aTjtddkSK9o)
5.    Introduction to Neural Networks
    * Lecture notes
6.    Gradient descent and Backpropagation
    * Lecture notes
7.    Convolutional Neural Networks
    * Lecture notes
8.    Introduction to TensorFlow
    * Lecture notes
9.    TensorFlow practice
    * [Nordling Lab - Python and Deep learning tutorial in Colaboratory](https://colab.research.google.com/drive/1uj3GEmGEJbJeaZEnJiUW0aTjtddkSK9o)
10.    TensorFlow practice
    * [Nordling Lab - Python and Deep learning tutorial in Colaboratory](https://colab.research.google.com/drive/1uj3GEmGEJbJeaZEnJiUW0aTjtddkSK9o)
11.    TensorFlow practice
    * [Nordling Lab - Python and Deep learning tutorial in Colaboratory](https://colab.research.google.com/drive/1uj3GEmGEJbJeaZEnJiUW0aTjtddkSK9o)
12.    Automation and the future of work
    * Lecture notes
13.    Ethics and the danger of algorithm bias
    1. [How we can build AI to help humans, not hurt us](https://www.ted.com/talks/margaret_mitchell_how_we_can_build_ai_to_help_humans_not_hurt_us) [(zh)](https://www.youtube.com/watch?v=twWkGt33X_k&t=17s) (9:56). Margaret Mitchell says: "All that we see now is a snapshot in the evolution of artificial intelligence." "If we want AI to evolve in a way that helps humans, then we need to define the goals and strategies that enable that path now."
    2. [How I'm fighting bias in algorithms](https://www.ted.com/talks/joy_buolamwini_how_i_m_fighting_bias_in_algorithms) [(zh)](https://www.youtube.com/watch?v=UG_X_7g63rY) (8:44). Joy Buolamwini's eye-opening TED talk on biases and the need for accountability and understanding of why a decision was made.
    3. [Fake videos of real people -- and how to spot them (zh)](https://www.youtube.com/watch?v=o2DDU4g0PRo) (7:15). "Do you think you're good at spotting fake videos, where famous people say things they've never said in real life? See how they're made in this astonishing talk and tech demo. Computer scientist Supasorn Suwajanakorn shows how, as a grad student, he used AI and 3D modeling to create photorealistic fake videos of people synced to audio."
    4. [AI "Stop Button" Problem - Computerphile (en)](https://www.youtube.com/watch?v=3TYT1QfdfsM) (19:59). Rob Miles explains the perils of implementing an on/off switch on a General Artificial Intelligence. 
    5. [Stop Button Solution? - Computerphile (none)](https://www.youtube.com/watch?v=9nktr1MgS-A) (23:44). "Rob Miles takes a look at a promising solution: Cooperative Inverse Reinforcement Learning."
14.    Application of Deep Learning (group project)
15.    Application of Deep Learning (group project)
16.    Application of Deep Learning (group project)
17.    Final exam and group presentations

# Homework assigments (2019) #

Please see the lecture notes in the course Google Drive if no assignment is listed here. Unless otherwise stated the assignment should be completed before the next lecture.

1.    Success stories of Artificial Intelligence
    1. Sign up to participate in the course and get access to the material by registering in [this Google Sheet](https://docs.google.com/spreadsheets/d/1hlizmVXF8F3ppyZS7bEl_VTyuMQqR2ug6-24uDl77y8/).
    2. Provide feedback on the lecture by voting announymously in [Voto](http://m.voto.se/Lecture) within 24 hours after the lecture ended.
    3. Find a success story where an AI outperform humans
        1. Read the success stories added in the folder SuccessStories in the [course Google Drive](https://drive.google.com/drive/folders/1HBzlkQm8hiOoJ_cFX9bFXbISjMYK-RR4).
        2. Find one additional unique success story where an AI outperform humans or at least is useful for performing a specific task. Use [Google](https://www.google.com/), [Google Scholar](https://scholar.google.com/), [Scopus](https://www.scopus.com/search/form.uri?display=basic), [Arxiv](https://arxiv.org/search/) to find information about the AI.
        3. Prepare 1-3 PPT/KEY slides about the AI using the Nordling Lab template available [here](https://drive.google.com/open?id=1svIqjZkx0IaJ7hZUGhZhs4eGmd2qZGh7). Focus on showing pictures of which problem the AI solves and how the performance of the AI compare to humans.
        4. Upload the slides to the folder SuccessStories in the [course Google Drive](https://drive.google.com/drive/folders/1HBzlkQm8hiOoJ_cFX9bFXbISjMYK-RR4). (Do not email any files to me.)
        5. Use a filename on the format YourFamilyName_YourStudentNumber_Short_description_of_success_story, e.g. Wu_E1000000_Image_captions.pptx.
        6. Ensure that you picked a different success story than all the previously added ones in the folder.
        7. Be prepared to present your success story to the class next time.
2.    What is AI, Deep Learning, and Machine Learning?
    1. Start writing your weekly diary in accordance with [these diary instructions](https://bitbucket.org/temn/nordlinglab-introai/src/master/README_DIARY.md). Note that you from now on until the last lecture are expected to add an entry to your personal diary during or after each lecture. Your attendance score is counted based on your diary entries. (Note: you need to write the diary entry for last week's lecture in addition to this week's. Please follow the correct format while doing so. Further detailed instructions can be found in ['README_DIARY.md'](https://bitbucket.org/temn/nordlinglab-introai/src/master/README_DIARY.md) -> 'What to write in your diary?')
3.    Introduction to Python
4.    Python practice
    1. Do all exercises, except the ones related to TensorFlow, in [Nordling Lab - Python and Deep learning tutorial in Colaboratory](https://colab.research.google.com/drive/1uj3GEmGEJbJeaZEnJiUW0aTjtddkSK9o).
5.    Introduction to Neural Networks
    1. Read Ch. 5.1-5.3 in [Goodfellow, I., Bengio, Y. & Courville, A., 2016. Deep learning](http://www.deeplearningbook.org)
    1. Watch the 3blue1brown visualisation on neural networks - ["But what *is* a Neural Network? | Chapter 1"](https://youtu.be/aircAruvnKk)
6.    Gradient descent and Backpropagation
    1. Read Ch. 5.9 in [Goodfellow, I., Bengio, Y. & Courville, A., 2016. Deep learning](http://www.deeplearningbook.org)
    1. Watch the 3blue1brown visualisations on gradient descent - ["Gradient descent, how neural networks learn | Chapter 2"](https://youtu.be/IHZwWFHWa-w), backpropagation - ["What is backpropagation really doing? | Chapter 3"](https://youtu.be/Ilg3gGewQ5U), and calculus of backpropagation and the chain rule - ["Backpropagation calculus | Appendix to deep learning chapter 3"](https://youtu.be/tIeHLnjs5U8) based on the MINST example in [Neural Networks and Deep Learning](http://neuralnetworksanddeeplearning.com/) by Michael Nielsen.
7.    Convolutional Neural Networks
    1. Read Ch. 9.1-9.3 in [Goodfellow, I., Bengio, Y. & Courville, A., 2016. Deep learning](http://www.deeplearningbook.org)
    1. Watch Brandon Rohrer's ["How Convolutional Neural Networks work"](https://www.youtube.com/watch?v=FmpDIaiMIeA) from 2:00 to 16:44.
8.    Introduction to TensorFlow
    1. Watch Martin Görner’s ["TensorFlow and Deep Learning without a PhD"](https://youtu.be/u4alGiomYP4).
9.    TensorFlow practice
    1. Finish the TensorFlow exercises in [Nordling Lab - Python and Deep learning tutorial in Colaboratory](https://colab.research.google.com/drive/1uj3GEmGEJbJeaZEnJiUW0aTjtddkSK9o).
10.    TensorFlow practice
    1. Write in your diary what makes Deep Learning difficult for you.
11.    TensorFlow practice
    1. Finish the TensorFlow exercises in [Nordling Lab - Python and Deep learning tutorial in Colaboratory](https://colab.research.google.com/drive/1uj3GEmGEJbJeaZEnJiUW0aTjtddkSK9o).
12.    Automation and the future of work
    1. Write your thoughts about automation and your future work plans in your diary. 
13.    Ethics and the danger of algorithm bias
    1. Read ["Using neural nets to recognize handwritten digits" in Neural Networks and Deep Learning by Michael Nielsen](http://neuralnetworksanddeeplearning.com/chap1.html).
14.    Application of Deep Learning (group project)
    1. Group division has been added in [Google sheet for signing up and reviewing scores](https://docs.google.com/spreadsheets/d/1hlizmVXF8F3ppyZS7bEl_VTyuMQqR2ug6-24uDl77y8/edit?usp=sharing). The people with the same number in the Group name column belong to the same group. Please contact your group member and select one person as group leader. Inform the teacher if some member in your group is not going to do the group work by email.
    1. Your task is to make the best possible deep neural network for classification of digits. (If it also can recognise all letters in the English alphabet, then you will get a bonus of 5%.) Feel free to take inspiration from the models included in the following benchmarks or any published article: [Benchmarks.AI](https://benchmarks.ai/mnist), [rodrigob.github.io](https://rodrigob.github.io/are_we_there_yet/build/classification_datasets_results.html). You are allowed to have exactly the same model structure as a published model, but you must train it yourself and demonstrate the predictive ability yourself.
    1. Alternatively, you are welcome to do online learning of a deep neural network for classification of digits using as few samples as possible for training, see e.g. [MIPRCV benchmarks MNIST](http://dag.cvc.uab.es/mnist/).
    1. You need to present your results by running the digit recognition on the test set of MNIST in a [Jupyter notebook](https://jupyter.org/) in [Colaboratory](https://colab.research.google.com/). 
    1. Your [Jupyter notebook](https://jupyter.org/) with both the training and prediction code, as well as the model specification and final model (saved in HDF5 format), need to be commited to this GIT repository before the last lecture. Please name your files MNIST_Error%_FirstName_FamilyName after the group leader.
15.    Application of Deep Learning (group project)
16.    Application of Deep Learning (group project)
    1. Check your weekly diary score at [Statistics of GIT commits](http://statistics.nordlinglab.org/introai/). If you notice any error, then please report it by email to *rain.wu@nordlinglab.org*.
17.    Final exam and group presentations




